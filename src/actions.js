import { Query } from "react-apollo";
import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";

const GET_ITEMS = gql`
	query {
		GetItems {
			id
			name
			price
		}
	}
`;

// const ADD_ITEM = gql`
// 	mutation Mutation($type: int!) {
// 		AddItem(id: $type) {
// 			id
// 		}
// 	}
// `;

// const AddItem = () => (
// 	<Mutation mutation={ADD_ITEM}>
// 	{(AddItem, { data }) => (console.log(data))}
// 	</Mutation>
// );

const getItems = () => (
	<Query query={GET_ITEMS}>
		{({ loading, error, data }) => {
			if (error) return "Error";
			if (loading || !data) return "Loading...";
			else {
				return (
					<ul>
						{data.GetItems.map(item => (
							<li key={item.id}>
								Name: {item.name}, Price: {item.price}
								<button>
									Add to backet
									{/*to do */}
								</button>
							</li>
						))}
					</ul>
				);
			}
		}}
	</Query>
);

export { getItems };
