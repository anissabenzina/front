import React, { Component } from "react";
import "./App.css";
import { getItems } from "./actions";

class App extends Component {
	// componentDidMount() {

	// }

	render() {
		return (
			<div className="App">
				<header className="App-header">Small App</header>
				<div className="data">{getItems()}</div>
			</div>
		);
	}
}

export default App;
